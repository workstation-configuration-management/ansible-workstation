#!/bin/bash

# run with:
# curl -L -k https://gitlab.com/workstation-configuration-management/ansible-workstation/-/raw/master/init.sh | sudo bash

# exit if a command fails
set -e

REPO=${REPO:-https://gitlab.com/workstation-configuration-management/ansible-workstation.git}
CLONE_DIR=${CLONE_DIR:-/var/lib/ansible/local}

function install_base_packages {
    apt-get install -y \
        ansible \
        git \
        python3-apt \
        python3-selinux
}

function install_requirements {
    ansible-galaxy collection install community.general ansible.posix
    #ansible-galaxy install -f requirements.yml
}

function run_playbook {
    ansible-pull --accept-host-key --clean --full --purge -o -C master -d ${CLONE_DIR} -i ${CLONE_DIR}/hosts.ini -l localhost -U ${REPO}
}

function main {
    install_base_packages
    install_requirements
    run_playbook
}

main
