# ansible-workstation

Roles and playbooks that allow provisioning personal computers

## Execution

curl -L -k https://gitlab.com/workstation-configuration-management/ansible-workstation/-/raw/master/init.sh | sudo bash

## TODO:
- ssh keys
- ansible user wih sudoers and no password
- molecule tests
- add lynis and openscap
- ssd conditional
- fw
- firefox verify integrity places (https://easylinuxtipsproject.blogspot.com/p/speed-mint.html) 
- disable bluetooth (check above link)
- put /tmp on tmpfs (check if tmp is small enough) check link above - https://ubuntu.com/blog/data-driven-analysis-tmp-on-tmpfs
- Ambient Light Sensor 
- check if vm.dirty_ratio = 10 is needed (check with grep dirty /proc/vmstat) (https://wiki.archlinux.org/index.php/Sysctl#Virtual_memory)
- https://wiki.archlinux.org/index.php/Profile-sync-daemon run bowser in tempfs and https://wiki.archlinux.org/index.php/Firefox/Profile_on_RAM
- 

## References

based on: 
- https://github.com/crivetimihai/ansible_workstation
- https://opensource.com/article/18/3/manage-workstation-ansible
- https://github.com/rothgar/ansible-workstation
